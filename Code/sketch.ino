#include <Stepper.h>
#include <LiquidCrystal_I2C.h>
#include "pitches.h"

int state = 1;

#define LED1 13
#define LED2 12
#define LED3 11
#define LED4 10
#define LED5 9
#define BUZZER 7
#define BUTTON1 17
#define BUTTON2 18
#define BUTTON3 19

const int stepsPerRevolution = 200;

Stepper pump1(stepsPerRevolution, 29, 27, 25, 23);
Stepper pump2(stepsPerRevolution, 37, 35, 33, 31);
Stepper pump3(stepsPerRevolution, 47, 45, 43, 41);
LiquidCrystal_I2C lcd(0x27,20,4);

int song[] = {
  //Take on me - a-ha
  NOTE_FS5, NOTE_FS5, NOTE_D5, NOTE_B4, NOTE_B4, NOTE_E5, 
  NOTE_E5, NOTE_E5, NOTE_GS5, NOTE_GS5, NOTE_A5, NOTE_B5, 
  NOTE_A5, NOTE_A5, NOTE_A5, NOTE_E5, NOTE_D5, NOTE_FS5, 
  NOTE_FS5, NOTE_FS5, NOTE_E5, NOTE_E5, NOTE_FS5, NOTE_E5
  
};
int durationList[] = {
  8, 8, 8, 4, 4, 4, 
  4, 5, 8, 8, 8, 8, 
  8, 8, 8, 4, 4, 4, 
  4, 5, 8, 8, 8, 8
};
int tempo = 200;
int songLength = sizeof(song) / sizeof(song[0]);
int duration = 0;

int ledPins[] = {LED1, LED2, LED3, LED4, LED5};

bool active = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
  pinMode(BUTTON3, INPUT_PULLUP);

  pump1.setSpeed(60);
  pump2.setSpeed(60);
  pump3.setSpeed(60);
  lcd.init();  
  lcd.backlight();
  setDisplay();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(!digitalRead(BUTTON1) && !active){
    state--;
    if(state < 1){
      state = 3;
    }
    setDisplay();
  }
  if(!digitalRead(BUTTON3) && !active){
    state++;
    if(state > 3){
      state = 0;
    }
    setDisplay();
  }
  if(!digitalRead(BUTTON2) && !active){
    if(state == 1){
      active = true;
      makeDrink();
    }
    else if(state == 2){
      active = true;
      playMusic();
    }
    else{
      active = true;
      lightShow(2);
    }
  }
}
void setDisplay(){
  lcd.clear();
  lcd.setCursor(2,0);
  lcd.print("Afterwork robot");
  lcd.setCursor(0,1);
  if(state == 1){
    lcd.print("1.Mix Drink X");
  }
  else{
    lcd.print("1.Mix Drink");
  }
  lcd.setCursor(0,2);
  if(state == 2){
    lcd.print("2.Turn on music X");
  }
  else{
    lcd.print("2.Turn on music");
  } 
  lcd.setCursor(0,3);
  if(state == 3){
    lcd.print("3.Turn on Lights X");
  }
  else{
    lcd.print("3.Turn on Lights");
  }
}

void makeDrink(){
  lcd.clear();
  lcd.setCursor(1,0);
  lcd.print("Making Mannhattan");
  lcd.setCursor(2,1);
  lcd.print("Pouring Whiskey!");
  pump1.step(stepsPerRevolution * 10);
  delay(500);
  lcd.setCursor(1,1);
  lcd.print("Pouring Vermouth!");
  pump2.step(stepsPerRevolution * 4);
  delay(200);
  lcd.setCursor(1,1);
  lcd.print("Pouring Angostura!");
  pump3.step(stepsPerRevolution / 10);
  delay(500);
  lcd.setCursor(1,1);
  lcd.print("                  ");
  lcd.setCursor(1,1);
  lcd.print("Mixing!");
  delay(4000);
  lcd.setCursor(1,1);
  lcd.print("                 ");
  lcd.setCursor(1,1);
  lcd.print("Adding cherry!");
  delay(1000);
  lcd.setCursor(1,1);
  lcd.print("                 ");
  lcd.setCursor(1,1);
  lcd.print("Enjoy!");
  delay(3000);
  setDisplay();
  active = false;
}

void playMusic(){
  lcd.clear();
  lcd.setCursor(7,1);
  lcd.print("Party!");
  for (int thisNote = 0; thisNote < songLength; thisNote++) {
    duration = 1000/durationList[thisNote];

    tone(BUZZER, song[thisNote], duration*0.9);
    delay(duration * 1.3);
    noTone(BUZZER);
  }
  setDisplay();
  active = false;
}

void lightShow(int repeat){
  lcd.clear();
  lcd.setCursor(7,1);
  lcd.print("Party!");
  while(repeat > 0){
    // flash in a wave two times
    for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++){
        digitalWrite(ledPins[j], HIGH);
        delay(50);
        digitalWrite(ledPins[j], LOW);
        delay(50);
      }
      for(int k = 4; k >= 0; k--){
        digitalWrite(ledPins[k], HIGH);
        delay(50);
        digitalWrite(ledPins[k], LOW);
        delay(50);
      }
    }
    for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++){
        digitalWrite(ledPins[j], HIGH);
      }
      delay(500);
      for(int j = 0; j < 5; j++){
        digitalWrite(ledPins[j], LOW);
      }
      delay(500);
    }
    for(int i = 0; i < 5; i++){
      digitalWrite(ledPins[0], HIGH);
      digitalWrite(ledPins[2], HIGH);
      digitalWrite(ledPins[4], HIGH);
      digitalWrite(ledPins[1], LOW);
      digitalWrite(ledPins[3], LOW);
      delay(500);
      digitalWrite(ledPins[0], LOW);
      digitalWrite(ledPins[2], LOW);
      digitalWrite(ledPins[4], LOW);
      digitalWrite(ledPins[1], HIGH);
      digitalWrite(ledPins[3], HIGH);
      delay(500);
    }
    digitalWrite(ledPins[0], LOW);
    digitalWrite(ledPins[2], LOW);
    digitalWrite(ledPins[4], LOW);
    digitalWrite(ledPins[1], LOW);
    digitalWrite(ledPins[3], LOW);
    repeat--;
  }
  setDisplay();
  active = false;
}